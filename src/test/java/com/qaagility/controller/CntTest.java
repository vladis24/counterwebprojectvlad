package com.qaagility.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CntTest
{
	@Test
	public void testDivision() throws Exception
	{
		Cnt cnt =  new Cnt();
		assertEquals(8, cnt.d(24, 3));
	}
	
	@Test
	public void testDivisionbyZero() throws Exception
	{
		Cnt cnt =  new Cnt();
		assertEquals(Integer.MAX_VALUE, cnt.d(24, 0));
	}

}
